<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    <div class="wrap p-3">
      <div class="wrap-inner p-3">
        @php(do_action('get_header'))
        @include('partials.header')
        <div class="container pt-2 pt-sm-5" role="document">
          <div class="content row justify-content-center">
            <main class="main col-md-9">
              @yield('content')
            </main>
            @if (App\display_sidebar())
              <aside class="sidebar">
                @include('partials.sidebar')
              </aside>
            @endif
          </div>
        </div>
        @php(do_action('get_footer'))
        @include('partials.footer')
        @php(wp_footer())
      </div>
    </div>
  </body>
</html>
