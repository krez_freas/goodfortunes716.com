@php(the_content())

<div>
@foreach(get_field('section') as $section)
  <div class="h1 display-3 mt-5">
    {{$section['title']}}
  </div>
  @foreach($section['menu_items'] as $item)
    @if($item['acf_fc_layout'] === 'menu_item')
      @include('partials.menu.menu-item')
    @endif
    @if($item['acf_fc_layout'] === 'table')
      @include('partials.menu.table', ['table' => $item['table']])
    @endif
    @if($item['acf_fc_layout'] === 'subheader')
      <p class="text-uppercase large">{{$item['title']}}</p>
    @endif
  @endforeach
@endforeach

</div>
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
