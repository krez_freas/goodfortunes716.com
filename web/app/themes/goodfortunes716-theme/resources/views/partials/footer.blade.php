<footer class="content-info mt-2 pt-2 mt-sm-5 pt-sm-5">
	<div class="container">
		<div class="d-sm-flex justify-content-around">
			<div class="px-sm-2">
        <a href="tel:(716) 444-5004">(716) 444-5004</a>
        <br>
        Delivery by
				<a href="https://www.skipthedishes.com/" target="_blank">Skip the Dishes</a> and
				<a href="https://www.ubereats.com/" target="_blank">Uber Eats</a>
        <br>
				<a href="https://goo.gl/maps/zk7RGjDqQw62" target="_blank">11 Minnesota Avenue, Buffalo</a>
			</div>
			<div class="px-sm-2 mt-3 mt-sm-0">
				Monday - Closed
				<br> Tuesday - Thursday 10:30am - 9:00pm
				<br> Friday & Saturday 10:30am - 11:00pm
				<br> Sunday 11:00 - 7:00pm
			</div>
		</div>

	</div>
</footer>
