<header class="banner d-sm-flex justify-content-between align-items-center pb-3 mb-5 container">
  <a class="brand w-100" href="{{ home_url('/') }}"><img class="logo" src="@asset('images/logo.svg')" alt=""></a>
  <nav class="nav-primary w-100">
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
    @endif
  </nav>
</header>
