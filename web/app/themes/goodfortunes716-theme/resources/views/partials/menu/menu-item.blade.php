<div class="d-flex justify-content-between">
  <div class="pr-5">
    <span class="h2">
      {{$item['title']}}
    </span>
    <p>
      {{$item['description']}}
      <br>
      <span class="small font-weight-bold">{{$item['footnote']}}</span>
    </p>
  </div>
  <div>
    @foreach($item['price'] as $price)
      <div class="d-flex justify-content-end align-items-baseline">
        <span>{{$price['description']}}</span> <span class="menu-price">{{$price['price']}}</span>
      </div>
    @endforeach
  </div>
</div>
