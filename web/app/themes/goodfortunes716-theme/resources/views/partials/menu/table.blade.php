<table style="margin-bottom: 2rem; margin-top: -1.5rem">
  <tbody>
    @foreach($table['body'] as $tr)
      <tr>
			  @foreach($tr as $td)
          <td class="pr-5">
            {!!$td['c']!!}
          </td>
			  @endforeach
        </tr>
		@endforeach
  </tbody>
</table>
